# drop script for Dropbox (based on rsync)

Put the `drop` script into your `~/bin` folder, and `include`/`exclude` files into `~/Dropbox/filters`
Now you can do selective copy.
You can change default destination (variable `DEFDEST`) or default filter path (`FILTERPATH`)
Here are some examples of use:
 
    drop
copies the current directory to `~/Dropbox` according to patterns in `~/Dropbox/filters`

    drop -h
displays help-info

    drop -f
copies the current directory to `~/Dropbox` according to patterns in `~/Dropbox/filters`, overwrites newer files 

    drop latex ~/Dropbox/Docs
copies the `latex` directory to `~/Dropbox/Docs` according to patterns in `~/Dropbox/filters`

    drop latex
copies the `latex` directory to a default directory (`~/Dropbox`) according to patterns in `~/Dropbox/filters`

    drop -an latex
copies only the directory-structure of the `latex` directory to a default directory (`~/Dropbox`)

    drop -i "*.log" latex
copies the `latex` directory to a default directory (`~/Dropbox`) according to patterns in `~/Dropbox/filters` including all `log`-files

    drop -ani "*tex *.pdf" latex
copies the `latex` directory with all `tex` and `pdf` files to a default directory (`~/Dropbox`) 

## Alias-example:
    alias droptex='drop -x "*.dvi" -D latex'
copy the current directory (or explicitly given one) to `latex`-directory in default directory (`~/Dropbox/latex`), exclude `.dvi` and other stuff in `~/Dropbox/filters/exclude`

## Pick

If you change the name of script to `pick` (e.g. by symlinking), the behaviour will be opposite, i.e. the files will be picked up instead of dropping. 

## Desktop link
You can create a desktop file for drag'n'drop copy files and directories with exclusion.
Here are two examples:
`Dropbox.desktop`:

    [Desktop Entry]
    Version=1.0
    Name=Dropbox
    Comment=Drop file here
    GenericName=Drop Script
    Exec=sh -c 'if [[ "%F" != "" ]]; then drop %F; else echo Drop files on the icon!; sleep 3; fi'
    Terminal=true
    Type=Application
    Icon=dropbox.png
    Categories=Utilities;

copies files and folders dropped on the icon to default drop directory (`~/Dropbox`) according to patterns in `~/Dropbox/filters`. 

`DropTeX.desktop`:

    [Desktop Entry]
    Version=1.0
    Name=DropTeX
    Comment=Drop file here to copy to ~/Dropbox/latex
    GenericName=Drop Script
    Exec=sh -c 'if [[ "%F" != "" ]]; then drop -x "*.dvi" -D latex %F; else echo Drop files on the icon to copy to ~/Dropbox/latex!; sleep 3; fi'
    Terminal=true
    Type=Application
    Icon=dropbox.png
    Categories=Utilities;

copies files and folders dropped on the icon to a `latex` folder in default drop directory (`~/Dropbox/latex`) according to patterns in `~/Dropbox/filters`.
